package ChatLab.client;


public class Controller {
	private Model model;
	private View view;
	

	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;
		
		view.connectButton.setOnAction(event -> {
			view.connectButton.setDisable(true);
			String ipAddress = view.ipTF.getText();
			int port = Integer.parseInt(view.portTF.getText());
			String name = view.nameTF.getText();
			model.connect(ipAddress, port, name);
		});
		
		view.stage.setOnCloseRequest(event -> model.disconnect() );
		
		view.sendButton.setOnAction(event -> model.sendMessage(view.commandTF.getText()));
		
		model.newestMessage.addListener( (o, oldValue, newValue) ->  {
		if( !newValue.isEmpty()) //Ignore empty messages
			view.textArea.appendText(newValue + "\n");
		} );
	
	}
	
	
	
	
	
}
