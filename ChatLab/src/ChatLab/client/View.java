package ChatLab.client;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class View {
	private Model model;
	protected Stage stage;
	
	protected Label ipAddressLbl, portLbl, nameLbl;
	protected TextField ipTF, portTF, nameTF, commandTF;
	protected Button connectButton, sendButton;
	protected TextArea textArea;
	
	public View(Stage stage, Model model) {
		this.stage = stage;
		this.model = model;
		
		VBox root = new VBox();
		this.textArea = new TextArea();
		root.getStyleClass().add("root");
		
		HBox hboxUp = new HBox();
		hboxUp.getStyleClass().add("hboxUp");
		this.ipAddressLbl = new Label ("IP Address");
		this.ipTF = new TextField();
		this.portLbl = new Label ("Port");
		this.portTF = new TextField();
		this.nameLbl = new Label ("Name");
		this.nameTF = new TextField();
		this.connectButton = new Button("Connect");
		hboxUp.getChildren().addAll(ipAddressLbl, ipTF, portLbl, portTF, nameLbl, nameTF, connectButton);
		
		HBox hboxDown = new HBox();
		hboxDown.getStyleClass().add("hboxDown");
		this.commandTF = new TextField();
		this.sendButton = new Button("Send");
		hboxDown.getChildren().addAll(commandTF, sendButton);
		HBox.setHgrow(commandTF, Priority.ALWAYS);	
		
		// Prevent labels and button from shrinking below their preferred size
		ipAddressLbl.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		portLbl.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		nameLbl.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		connectButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		sendButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		
		// Set sizes for top TextFields
		ipTF.setMinWidth(150); ipTF.setPrefWidth(150);
		portTF.setMinWidth(60); portTF.setPrefWidth(60);
		nameTF.setMinWidth(150); nameTF.setPrefWidth(150);
		
		
		root.getChildren().addAll(hboxUp, textArea, hboxDown);
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("client.css").toExternalForm());
		
		stage.setScene(scene);
		stage.setTitle("Client");
			
	}

	public void start() {
		stage.show();
		
		// Prevent resizing below initial size
		stage.setMinWidth(stage.getWidth());
		stage.setMinHeight(stage.getHeight());
		
	}
	

}
