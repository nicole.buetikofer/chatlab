package ChatLab.client;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application{
	private Controller controller;
	private View view;
	private Model model;

	public static void main(String[] args) {
		launch(args);
	}
	
	public void start (Stage stage) throws Exception{
		view = new View(stage, model);
		model = new Model();
		controller = new Controller(model, view);
		view.start();
		
	}

}
