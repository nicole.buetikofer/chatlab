package ChatLab.server;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class View {
	private Model model;
	protected Stage stage;
	
	protected Label portLbl;
	protected TextField portTF;
	protected Button startButton;
	protected TextArea textArea;
	protected Region topSpacer;
	
	public View(Stage stage, Model model) {
		this.stage = stage;
		this.model = model;
		
		VBox root = new VBox();
		root.getStyleClass().add("root");
		this.portLbl = new Label ("Port");
		portLbl.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		this.portTF = new TextField();
		portTF.setMinWidth(60);
		portTF.setPrefWidth(60);
		this.startButton = new Button ("Start");
		startButton.setMinSize(Button.USE_PREF_SIZE, Button.USE_PREF_SIZE);
		this.textArea = new TextArea();
		
		this.topSpacer = new Region();
		
		HBox hbox = new HBox();
		hbox.getStyleClass().add("hbox");
		HBox.setHgrow(topSpacer, Priority.ALWAYS);
		hbox.getChildren().addAll(portLbl, portTF, topSpacer, startButton);
		
		root.getChildren().addAll(hbox, textArea);
		 
		Scene scene = new Scene(root);
		scene.getStylesheets().add(getClass().getResource("server.css").toExternalForm());
		
		stage.setScene(scene);
		stage.setTitle("Server");
		
	}

	public void start() {
		stage.show();
		
		// Prevent resizing below initial size
		stage.setMinWidth(stage.getWidth());
		stage.setMinHeight(stage.getHeight());
	}

	public void updateClients() {
		StringBuffer sb = new StringBuffer();
		for (Client c : model.clients) {
			sb.append(c.toString());
			sb.append("\n");
		}
		textArea.setText(sb.toString());
	}

}
