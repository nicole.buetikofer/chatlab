package ChatLab.server;

import javafx.collections.ListChangeListener;
import javafx.scene.input.MouseEvent;

public class Controller {
	private Model model;
	private View view;
	
	public Controller(View view, Model model) {
		this.model = model;
		this.view = view;
		
		view.startButton.setOnAction(event -> {
			view.startButton.setDisable(true);
			int port = Integer.parseInt(view.portTF.getText());
			model.startServer(port);
		});
		
		view.stage.setOnCloseRequest(event -> model.stopServer());
		
		model.clients.addListener((ListChangeListener) (event -> view.updateClients()));
		
		
	}

	


}
